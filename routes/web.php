<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\BudgetingController;
use App\Http\Controllers\TransaksiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('register', [AuthController::class, 'registerPage']);
Route::get('login', [AuthController::class, 'loginPage']);

Route::post('register/process', [AuthController::class, 'registerProcess']);
Route::post('login/process', [AuthController::class, 'loginProcess']);

Route::middleware('auth.user')->group(function() {
    Route::get('sendcode/{id}', [EmailController::class, 'sendVerificationCode']);
    Route::get('user/verify', [AuthController::class, 'showVerifyPage']);
    Route::post('user/verify/process', [AuthController::class, 'verifyCode']);

    Route::middleware('auth.verify')->group(function () {
        Route::get('home', [HomeController::class, 'index']);
        Route::get('profile', [HomeController::class, 'profile']);

        Route::post('user/changepassword', [AuthController::class, 'changePassword']);
        Route::post('user/changeemail', [AuthController::class, 'changeEmail']);
        Route::post('user/changepicture', [AuthController::class, 'changeProfilePicture']);
        Route::get('kategori', [KategoriController::class, 'index']);
        Route::post('kategori/store', [KategoriController::class, 'store']);
        Route::get('kategori/{id}', [KategoriController::class, 'show']);
        Route::post('kategori/{id}/update', [KategoriController::class, 'update']);
        Route::get('kategori/{id}/delete', [KategoriController::class, 'destroy']);

        Route::get('budgeting', [BudgetingController::class, 'index']);
        Route::post('budgeting/{id}/update', [BudgetingController::class, 'update']);

        Route::get('transaksi', [TransaksiController::class, 'index']);
        Route::post('transaksi/store', [TransaksiController::class, 'store']);
        Route::post('transaksi/{id}/update', [TransaksiController::class, 'update']);
        Route::get('transaksi/{id}/delete', [TransaksiController::class, 'destroy2']);
    });

    Route::get('logout', [AuthController::class, 'logout']);
});

