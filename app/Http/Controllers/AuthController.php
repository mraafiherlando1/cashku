<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Mail\SendVerificationCode;

class AuthController extends Controller
{
    public function registerPage()
    {
        return view('auth.register');
    }
    public function loginPage()
    {
        return view('auth.login');
    }
    public function registerProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed'
        ]);
        if ($validator->fails()) {
            return redirect('register')->with('error', $validator->errors()->first());
        }

        $register = User::create([
            'name' => $request->nama,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'verification_code' => $this->generateVerificationCode()
        ]);

        Auth::login($register);

        return redirect('sendcode/' . $register->id);
    }

    public function loginProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
        $validated = $validator->validate();
        if ($validator->fails()) {
            return redirect('login')->with('error', $validator->errors());
        }

        if (Auth::attempt($validated)) {
            $user = User::where('email', $validated['email'])->first();
            Auth::login($user);
            return redirect('home');
        } else {
            return redirect('login')->with('error', 'Email atau password salah');
        }
    }

    public function showVerifyPage()
    {
        return view('auth.verify');
    }

    public function verifyCode(Request $request)
    {
        if (Auth::user()->verification_code == $request->verification_code) {
            $update = User::where('id', Auth::user()->id)->update([
                'is_verified' => 1
            ]);
            Auth::login(User::where('id', Auth::user()->id)->first());
            if (!$update) {
                return redirect('user/verify')->with('error', 'Verifikasi akun gagal, silakan coba lagi');
            }
            return redirect('/home');
        }
        return redirect('user/verify')->with('error', 'Kode verifikasi salah');
    }

    public function changeEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'new_email' => 'required|email|unique:users,email',
        ]);
        if ($validator->fails()) {
            return redirect('profile')->with('error', $validator->errors()->first());
        }

        $update = User::where('id', Auth::user()->id)->update([
            'email' => $request->new_email,
            'is_verified' => 0,
            'verification_code' => $this->generateVerificationCode()
        ]);
        return redirect('sendcode/' . Auth::user()->id);
    }

    public function changePassword(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if (Hash::check($request->old_password, $user->password)) {
            $update = User::where('id', Auth::user()->id)->update([
                'password' => Hash::make($request->new_password)
            ]);
            return redirect('profile')->with('success', 'berhasil mengubah password');
        }
        return redirect('profile')->with('error', 'Password lama tidak cocok');
    }

    public function changeProfilePicture(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'picture' => 'required|file|mimes:jpg,png,jpeg,svg',
        ]);
        if($validator->fails()){
            return redirect('profile')->with('error', $validator->errors()->first());
        }
        $user = User::find(Auth::user()->id);
        $delete = Storage::disk('public')->delete('profile/' . $user->profile_picture);
        $extension = $request->file('picture')->getClientOriginalExtension();
        $newName = Auth::user()->id . '.' . $extension;
        $store = $request->file('picture')->storeAs('profile', $newName, 'public');
        if ($store) {
            $update = User::where('id', Auth::user()->id)->update([
                'profile_picture' => $newName
            ]);
            return redirect('profile')->with('success', 'berhasil mengubah foto profil');
        }
        return redirect('profile')->with('error', 'gagal mengubah foto profil');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login')->with('success', 'berhasil logout');
    }

    public function generateVerificationCode()
    {
        $character = '1234567890';
        $randomString = '';
        $unique = false;

        while ($unique == false) {
            $randomString = substr(str_shuffle($character), 0, 6);
            $check = User::where('verification_code', $randomString)->count();
            if ($check == 0) {
                $unique = true;
            }
        }

        return $randomString;
    }
}
