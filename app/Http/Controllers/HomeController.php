<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaksi;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DateTime;
use DateInterval;

class HomeController extends Controller
{
    public function index(){
        $date = new DateTime(date('Y-m-01'));
        $transaksi = Transaksi::leftJoin('kategori', 'kategori.id', '=', 'transaksi.kategori_id')->where('transaksi.user_id', Auth::user()->id)->orderBy('tanggal_transaksi', 'desc')->get();
        $pendapatan = Transaksi::whereBetween('tanggal_transaksi', [$date->format('Y-m-d'), $date->add(new DateInterval('P30D'))->format('Y-m-d H:i:s')])->where('tipe', 'in')->
                      where('user_id', Auth::user()->id)->sum('jumlah');
        $pendapatanBulanLalu = Transaksi::whereMonth('tanggal_transaksi', date('m', strtotime("-1 month")))->where('tipe', 'in')->where('user_id', Auth::user()->id)->sum('jumlah');
        $transaksiAll = Transaksi::whereMonth('tanggal_transaksi', date('m'))->where('user_id', Auth::user()->id)->limit(5)->get();
        $dataIn = [];
        $dataOut = [];

        // Initialize the data arrays
        for ($week = 1; $week <= 5; $week++) {
            $dataIn[$week] = 0;
            $dataOut[$week] = 0;
        }

        foreach ($transaksiAll as $transaction) {
            $week = Carbon::parse($transaction->tanggal_transaksi)->weekOfMonth;
            if ($transaction->tipe == 'in') {
                $dataIn[$week] += $transaction->jumlah;
            } elseif ($transaction->tipe == 'out') {
                $dataOut[$week] += $transaction->jumlah;
            }
        }

        return view('home', ['transaksi' => $transaksi, 'pendapatan' => $pendapatan, 'pendapatanBulanLalu' => $pendapatanBulanLalu, 'dataMasuk' => $dataIn, 'dataKeluar' => $dataOut]);
    }

    public function profile(){
        $user = User::where('id', Auth::user()->id)->first();
        return view('profile', ['data' => $user]);
    }
}
