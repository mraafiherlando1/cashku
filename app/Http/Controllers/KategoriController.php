<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Models\Kategori;
use App\Models\Budgeting;
use App\Models\Transaksi;

class KategoriController extends Controller
{
    public function index()
    {
        $kategori = Kategori::where('user_id', Auth::user()->id)->get();

        return view('kategori.index', ['kategori' => $kategori]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_kategori' => 'required|string',
            'icon' => 'nullable|file|mimes:jpg,png,jpeg|max:1000',
        ]);
        if ($validator->fails()) {
            return redirect('kategori')->with('error', $validator->errors()->first());
        }

        $create = Kategori::create([
            'nama_kategori' => $request->nama_kategori,
            'icon' => $this->uploadFile($request->file('ikon'), $request->nama_kategori),
            'user_id' => Auth::user()->id
        ]);

        $budgeting = Budgeting::create([
            'jumlah_per_bulan' => 0,
            'kategori_id' => $create->id,
            'user_id' => Auth::user()->id
        ]);

        return redirect('kategori')->with('success', 'Berhasil menambahkan kategori');
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama_kategori' => 'required|string',
            'icon' => 'nullable|file|mimes:jpg,png,jpeg|max:1000',
        ]);
        if ($validator->fails()) {
            return redirect('kategori/' . $id)->with('error', $validator->errors()->first());
        }

        $findData = Kategori::find($id);
        if ($request->file('ikon') != null) {
            $delete = $this->deleteFile($request->file('ikon')->getClientOriginalName());
        }
        $update = Kategori::where('id', $id)->update([
            'nama_kategori' => $request->nama_kategori,
            'icon' => $request->file('ikon') != null ? $request->file('ikon') : $findData->icon
        ]);

        if ($update) {
            return redirect('kategori/' . $id)->with('success', 'berhasil memperbarui data');
        }
        return redirect('kategori/' . $id)->with('error', 'gagal memperbarui data');
    }

    public function destroy($id){
        $transaksi = Transaksi::where('kategori_id', $id)->delete();
        $find = Kategori::find($id);
        $deleteFile = $this->deleteFile($find->icon);
        if($deleteFile){
            $deleteRecord = Kategori::where('id', $id)->delete();
            if($deleteRecord){
                return redirect('kategori')->with('success', 'berhasil menghapus data kategori');
            }
        }
    }

    public function uploadFile($item, $nama_kategori)
    {
        if ($item == null) {
            return null;
        } else {
            $extension = $item->getClientOriginalExtension();
            $timestamps = str_replace(" ", "", str_replace("-","", date('d-m-Y H-i-s')));
            $filename = Auth::user()->id . '_' . $nama_kategori . '_' . $timestamps . '.' . $extension;
            $store = $item->storeAs('kategori', $filename, 'public');
            if (!$store) {
                Storage::disk('public')->delete('kategori/' . $filename);
                return null;
            }
            return $filename;
        }
    }

    public function deleteFile($filename)
    {
        $delete = Storage::disk('public')->delete('kategori/' . $filename);
        if ($delete) {
            return true;
        }
        return false;
    }
}
