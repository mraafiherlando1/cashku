<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Transaksi;
use App\Models\Kategori;
use Illuminate\Support\Facades\DB;


class TransaksiController extends Controller
{
    public function index()
    {
        $transaksiIn = Transaksi::where('user_id', Auth::user()->id)->where('tipe', 'in')->get();
        $transaksiOut = Transaksi::join('kategori', 'kategori.id', '=', 'transaksi.kategori_id')->where('transaksi.user_id', Auth::user()->id)->where('tipe', 'out')->
                        select('transaksi.id', 'kategori.nama_kategori', 'transaksi.tanggal_transaksi', 'transaksi.jumlah')->get();
        $totalDanaMasuk = Transaksi::where('user_id', Auth::user()->id)->where('tipe', 'in')->sum('jumlah');
        $totalDanaKeluar = Transaksi::where('user_id', Auth::user()->id)->where('tipe', 'out')->sum('jumlah');
        $kategori = Kategori::where('user_id', Auth::user()->id)->get();
        return view('transaksi.index', ['transaksiIn' => $transaksiIn, 'transaksiOut' => $transaksiOut, 'totalDanaMasuk' => $totalDanaMasuk, 'totalDanaKeluar' => $totalDanaKeluar, 'kategori' => $kategori]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kategori' => 'nullable|string|exists:kategori,id',
            'jumlah' => 'required|numeric',
            'tanggal_transaksi' => 'required|date'
        ]);
        if ($validator->fails()) {
            return redirect('transaksi')->with('error', $validator->errors()->first());
        }

        $create = Transaksi::create([
            'tipe' => strlen($request->kategori) == 0 ? 'in' : 'out',
            'kategori_id' => $request->kategori,
            'user_id' => Auth::user()->id,
            'tanggal_transaksi' => $request->tanggal_transaksi,
            'jumlah' => $request->jumlah,
        ]);

        return redirect('transaksi')->with('success', 'berhasil menambahkan data transaksi');
    }

    // public function show($id){
    //     $
    // }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'kategori' => 'nullable|string|exists:kategori,id',
            'jumlah' => 'required|numeric',
            'tanggal_transaksi' => 'nullable|date'
        ]);
        if ($validator->fails()) {
            return redirect('transaksi/')->with('error', $validator->errors()->first());
        }

        $find = Transaksi::find($id);

        $update = Transaksi::where('id', $id)->update([
            'tipe' => strlen($request->kategori) == 0 ? 'in' : 'out',
            'kategori_id' => $request->kategori,
            'user_id' => Auth::user()->id,
            'tanggal_transaksi' => $request->tanggal_transaksi == null ? $find->tanggal_transaksi : $request->tanggal_transaksi,
            'jumlah' => $request->jumlah,
        ]);
        if ($update) {
            return redirect('transaksi/')->with('success', 'berhasil memperbarui data transaksi');
        }
        return redirect('transaksi/')->with('error', 'gagal memperbarui data transaksi');
    }

    public function destroy($id)
    {
        $delete = DB::table('transaksi')->where('id', '=', $id)->destr();
        if ($delete) {
            return redirect('transaksi')->with('success', 'berhasil menghapus data');
        }
        return redirect('transaksi')->with('error', 'gagal menghapus data');
    }

    public function destroy2($id){
        $delete = Transaksi::where('id', '=', $id)->delete();
        if ($delete) {
            return redirect('transaksi')->with('success', 'berhasil menghapus data');
        }
        return redirect('transaksi')->with('error', 'gagal menghapus data');
    }
}
