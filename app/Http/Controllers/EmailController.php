<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Mail\SendVerificationCode;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function sendVerificationCode($id)
    {
        $userData = User::where('id', $id)->first();

        Mail::to($userData->email)->send(new SendVerificationCode($userData));

        return redirect('user/verify');
    }
}
