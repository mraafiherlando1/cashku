<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Budgeting;
use App\Models\Transaksi;
use DateTime;
use DateInterval;

class BudgetingController extends Controller
{
    public function index(){
        $budgeting = Budgeting::where('user_id', Auth::user()->id)->get();
        $date = new DateTime(date('Y-m-01'));
        $sum = 0;
        foreach($budgeting as $data){
            $sumExpense = Transaksi::where('kategori_id', $data->kategori_id)
                                    ->sum('jumlah');
            $data->total_pengeluaran = $sumExpense;
            $sum += $sumExpense;
        }
        $budgetTotal = Budgeting::where('user_id', Auth::user()->id)->sum('jumlah_per_bulan');
        return view('budgeting.index', ['budgeting' => $budgeting, 'total_budget' => $budgetTotal]);
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'jumlah_budget' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return redirect('budgeting')->with('error', $validator->errors()->first());
        }
        $find = Budgeting::find($id);
        $update = Budgeting::where('id', $id)->update([
            'jumlah_per_bulan' => $request->jumlah_budget
        ]);
        if($update){
            return redirect('budgeting')->with('success', 'berhasil mengubah budgeting untuk kategori '.$find->kategori->nama_kategori);
        }
        return redirect('budgeting')->with('error', 'gagal mengubah budgeting untuk kategori ' . $find->kategori->nama_kategori);
    }
}
