<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Concerns\HasUlids;

/**
 * Class User
 * 
 * @property string $id
 * @property string $name
 * @property string $username
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string $profile_picture
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|Budgeting[] $budgetings
 * @property Collection|Transaksi[] $transaksis
 *
 * @package App\Models
 */
class User extends Model implements Authenticatable
{
	use HasUlids;
	protected $table = 'users';
	public $incrementing = false;

	protected $casts = [
		'email_verified_at' => 'datetime'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'name',
		'email',
		'email_verified_at',
		'password',
		'profile_picture',
		'remember_token',
		'verification_code',
		'is_verified'
	];

	public function budgetings()
	{
		return $this->hasMany(Budgeting::class);
	}

	public function transaksis()
	{
		return $this->hasMany(Transaksi::class);
	}

	public function getAuthIdentifierName()
	{
		return 'id';
	}

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the remember token for the user.
	 *
	 * @return string|null
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the remember token for the user.
	 *
	 * @param  string|null  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}
}
