<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUlids;

/**
 * Class Transaksi
 * 
 * @property string $id
 * @property int $jumlah
 * @property string $kategori_id
 * @property string $user_id
 * @property Carbon $tanggal_transaksi
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Kategori $kategori
 * @property User $user
 *
 * @package App\Models
 */
class Transaksi extends Model
{
	use HasUlids;
	protected $table = 'transaksi';
	public $incrementing = false;

	protected $casts = [
		'jumlah' => 'int',
		'tanggal_transaksi' => 'datetime'
	];

	protected $fillable = [
		'tipe',
		'jumlah',
		'kategori_id',
		'user_id',
		'tanggal_transaksi'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
