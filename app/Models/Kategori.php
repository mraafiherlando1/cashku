<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUlids;

/**
 * Class Kategori
 * 
 * @property string $id
 * @property string $nama_kategori
 * @property string $icon
 * @property bool $is_general
 * @property string|null $user_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|Budgeting[] $budgetings
 * @property Collection|Transaksi[] $transaksis
 *
 * @package App\Models
 */
class Kategori extends Model
{
	use HasUlids;
	protected $table = 'kategori';
	public $incrementing = false;

	protected $casts = [
		'is_general' => 'bool'
	];

	protected $fillable = [
		'nama_kategori',
		'icon',
		'is_general',
		'user_id'
	];

	public function budgetings()
	{
		return $this->hasMany(Budgeting::class);
	}

	public function transaksis()
	{
		return $this->hasMany(Transaksi::class);
	}
	
}
