<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUlids;

/**
 * Class Budgeting
 * 
 * @property string $id
 * @property int $jumlah_per_bulan
 * @property string $kategori_id
 * @property string $user_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Kategori $kategori
 * @property User $user
 *
 * @package App\Models
 */
class Budgeting extends Model
{
	use HasUlids;
	protected $table = 'budgeting';
	public $incrementing = false;

	protected $casts = [
		'jumlah_per_bulan' => 'int'
	];

	protected $fillable = [
		'jumlah_per_bulan',
		'kategori_id',
		'user_id'
	];

	public function kategori()
	{
		return $this->belongsTo(Kategori::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
