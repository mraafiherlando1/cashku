@extends('template.template')

@section('content')
    <div class="row">
        @if (session('error'))
            <div class="response error">
                <p class="text-center text-white my-auto">{{ session('error') }}</p>
            </div>
        @endif
        @if (session('success'))
            <div class="response success">
                <p class="text-center text-white my-auto">{{ session('success') }}</p>
            </div>
        @endif
        <div class=" d-flex align-items-stretch">
            <div class="card w-100">
                <div class="card-body p-4">
                    <div class="d-flex justify-content-between">
                        <h5 class="card-title fw-semibold mb-4 text-center">Transaksi Keluar</h5>
                        <a href="" class="btn btn-primary btn-sm mb-4" data-bs-toggle="modal"
                            data-bs-target="#tambahTransaksiMasuk"><i class="ti ti-plus"></i></a>
                            <div class="modal fade" id="tambahTransaksiMasuk" tabindex="-1"
                                aria-labelledby="tambahTransaksiMasuk" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="exampleModalLabel">Tambah Transaksi Keluar</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form id="formLayananSettings" method="POST"
                                                action="{{ url('transaksi/store') }}"
                                                enctype="multipart/form-data">
                                                <div class="row">
                                                    @csrf
                                                    <div class="mb-3">
                                                        <label for="kategori" class="form-label">Kategori</label>
                                                        <select name="kategori" id="" class="select2 form-select" required>
                                                            @foreach($kategori as $data)
                                                            <option value="{{$data->id}}">{{$data->nama_kategori}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="nama" class="form-label">Jumlah</label>
                                                        <input class="form-control" type="number" name="jumlah"
                                                            id="pertanyaan" placeholder="Jumlah Transaksi keluar" required />
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="nama" class="form-label">Tanggal Transaksi</label>
                                                        <input class="form-control" type="date" name="tanggal_transaksi"
                                                            id="tanggalTransaksi" placeholder="Jumlah budget per bulan" required />
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-secondary"
                                                    data-bs-dismiss="modal">Tutup</button>
                                                <button type="submit" class="btn btn-primary">Simpan data</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                    <p>Total Dana Keluar : Rp{{ number_format($totalDanaKeluar, 2, ',', '.') }}</p>
                    <div class="table-responsive">
                        @if (sizeof($transaksiOut) == 0)
                            <p class="text-center">Tidak ada data transaksi keluar</p>
                        @else
                            <table class="table text-nowrap mb-0 align-middle">
                                <thead class="text-dark fs-4">
                                    <tr>
                                        <th class="border-bottom-0">
                                            <h6 class="fw-semibold mb-0">No</h6>
                                        </th>
                                        <th class="border-bottom-0">
                                            <h6 class="fw-semibold mb-0">Jumlah</h6>
                                        </th>
                                        <th class="border-bottom-0">
                                            <h6 class="fw-semibold mb-0">Kategori</h6>
                                        </th>
                                        <th class="border-bottom-0">
                                            <h6 class="fw-semibold mb-0">Tanggal Transaksi</h6>
                                        </th>
                                        <th class="border-bottom-0">
                                            <h6 class="fw-semibold mb-0">Aksi</h6>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i = 1 @endphp
                                    @foreach ($transaksiOut as $data)
                                        @php
                                            $date = new DateTime($data->tanggal_transaksi);
                                            setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
                                            $formattedDate = strftime('%e %B %Y', $date->getTimestamp());
                                        @endphp
                                        <tr>
                                            <td class="border-bottom-0">
                                                <h6 class="fw-semibold mb-0">{{ $i }}</h6>
                                            </td>
                                            <td class="border-bottom-0">
                                                <h6 class="fw-semibold mb-1 text-danger">-
                                                    Rp{{ number_format($data->jumlah, 2, ',', '.') }}</h6>
                                            </td>
                                            <td class="border-bottom-0">
                                                <h6 class="fw-semibold mb-1">{{ $data->nama_kategori }}</h6>
                                            </td>
                                            <td class="border-bottom-0">
                                                <h6 class="fw-semibold mb-1">{{ $formattedDate }}</h6>
                                            </td>
                                           <td class="border-bottom-0">
                                                <div class="d-flex gap-3">
                                                    <a class="btn btn-warning btn-sm text-white" data-bs-toggle="modal"
                                                        data-bs-target="#editTransaksiKeluar"><i class="ti ti-pencil"></i></a>
                                                    <div class="modal fade" id="editTransaksiKeluar" tabindex="-1"
                                                        aria-labelledby="editTransaksiKeluar" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h1 class="modal-title fs-5" id="exampleModalLabel">Edit Transaksi Keluar</h1>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form id="formLayananSettings" method="POST"
                                                                        action="{{ url("transaksi/".$data->id."/update") }}"
                                                                        enctype="multipart/form-data">
                                                                        <div class="row">
                                                                            @csrf
                                                                            <div class="mb-3">
                                                                                <input type="text" value="{{$data->id}}" hidden>
                                                                                <label for="kategori" class="form-label">Kategori</label>
                                                                                <select name="kategori" id="" class="select2 form-select" required>
                                                                                    @foreach($kategori as $data2)
                                                                                    @if($data2->id == $data->id)
                                                                                    <option value="{{$data2->id}}" selected>{{$data2->nama_kategori}}</option>
                                                                                    @else
                                                                                    <option value="{{$data2->id}}">{{$data2->nama_kategori}}</option>
                                                                                    @endif
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="mb-3">
                                                                                <label for="nama" class="form-label">Jumlah</label>
                                                                                <input class="form-control" type="number" name="jumlah"
                                                                                    id="pertanyaan" placeholder="Jumlah Transaksi keluar" value="{{$data->jumlah}}" required />
                                                                            </div>
                                                                            <div class="mb-3">
                                                                                <label for="nama" class="form-label">Tanggal Transaksi</label>
                                                                                <input class="form-control" type="date" name="tanggal_transaksi"
                                                                                    id="tanggalTransaksi" placeholder="Jumlah budget per bulan" value="{{$data->tanggal_transaksi}}" />
                                                                            </div>
                                                                        </div>
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                        <button type="submit" class="btn btn-primary">Simpan data</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="btn btn-danger btn-sm text-white" href="{{url('transaksi/'.$data->id.'/delete')}}"><i class="ti ti-trash"></i></a>
                                                </div>
                                            </td>

                                        </tr>
                                        @php ++$i @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class=" d-flex align-items-stretch">
            <div class="card w-100">
                <div class="card-body p-4">
                    <div class="d-flex justify-content-between">
                        <h5 class="card-title fw-semibold mb-4">Transaksi Masuk</h5>
                        <a class="btn btn-primary btn-sm mb-4" data-bs-toggle="modal"
                            data-bs-target="#tambahTransaksiKeluar"><i class="ti ti-plus"></i></a>
                            <div class="modal fade" id="tambahTransaksiKeluar" tabindex="-1"
                                aria-labelledby="tambahTransaksiKeluar" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="exampleModalLabel">Tambah Transaksi Masuk</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form id="formLayananSettings" method="POST"
                                                action="{{ url('transaksi/store') }}"
                                                enctype="multipart/form-data">
                                                <div class="row">
                                                    @csrf
                                                    <div class="mb-3">
                                                        <label for="nama" class="form-label">Jumlah</label>
                                                        <input class="form-control" type="number" name="jumlah"
                                                            id="pertanyaan" placeholder="Jumlah Transaksi masuk" required />
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="nama" class="form-label">Tanggal Transaksi</label>
                                                        <input class="form-control" type="date" name="tanggal_transaksi"
                                                            id="tanggalTransaksi2" placeholder="Jumlah budget per bulan" required />
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-secondary"
                                                    data-bs-dismiss="modal">Tutup</button>
                                                <button type="submit" class="btn btn-primary">Simpan data</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                    <p>Total Dana Masuk : Rp{{ number_format($totalDanaMasuk, 2, ',', '.') }}</p>
                    <div class="table-responsive">
                        @if (sizeof($transaksiIn) == 0)
                            <p class="text-center">Tidak ada data transaksi masuk</p>
                        @else
                            <table class="table text-nowrap mb-0 align-middle">
                                <thead class="text-dark fs-4">
                                    <tr>
                                        <th class="border-bottom-0">
                                            <h6 class="fw-semibold mb-0">No</h6>
                                        </th>
                                        <th class="border-bottom-0">
                                            <h6 class="fw-semibold mb-0">Jumlah</h6>
                                        </th>
                                        <th class="border-bottom-0">
                                            <h6 class="fw-semibold mb-0">Tanggal Transaksi</h6>
                                        </th>
                                        <th class="border-bottom-0">
                                            <h6 class="fw-semibold mb-0">Aksi</h6>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i = 1 @endphp
                                    @foreach ($transaksiIn as $data)
                                    @php
                                            $date = new DateTime($data->tanggal_transaksi);
                                            setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
                                            $formattedDate = strftime('%e %B %Y', $date->getTimestamp());
                                        @endphp
                                        <tr>
                                            <td class="border-bottom-0">
                                                <h6 class="fw-semibold mb-0">{{ $i }}</h6>
                                            </td>
                                            <td class="border-bottom-0">
                                                <h6 class="fw-semibold mb-1 text-success">+
                                                    Rp{{ number_format($data->jumlah, 2, ',', '.') }}</h6>
                                            </td>
                                            <td class="border-bottom-0">
                                                @php $date = new DateTime($data->created_at) @endphp
                                                <h6 class="fw-semibold mb-1">{{ $formattedDate }}</h6>
                                            </td>
                                            <td class="border-bottom-0">
                                                <div class="d-flex gap-3">
                                                    <a class="btn btn-warning btn-sm text-white" data-bs-toggle="modal"
                                                        data-bs-target="#editTransaksiMasuk"><i class="ti ti-pencil"></i></a>
                                                    <div class="modal fade" id="editTransaksiMasuk" tabindex="-1"
                                                        aria-labelledby="editTransaksiMasuk" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h1 class="modal-title fs-5" id="exampleModalLabel">Edit Transaksi Masuk</h1>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form id="formLayananSettings" method="POST"
                                                                        action="{{ url("transaksi/".$data->id."/update") }}"
                                                                        enctype="multipart/form-data">
                                                                        <div class="row">
                                                                            @csrf
                                                                            <div class="mb-3">
                                                                                <label for="nama" class="form-label">Jumlah</label>
                                                                                <input class="form-control" type="number" name="jumlah"
                                                                                    id="pertanyaan" placeholder="Jumlah Transaksi keluar" value="{{$data->jumlah}}" required />
                                                                            </div>
                                                                            <div class="mb-3">
                                                                                <label for="nama" class="form-label">Tanggal Transaksi</label>
                                                                                <input class="form-control" type="date" name="tanggal_transaksi"
                                                                                    id="tanggalTransaksi" placeholder="Jumlah budget per bulan" value="{{$data->tanggal_transaksi}}" />
                                                                            </div>
                                                                        </div>
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                        <button type="submit" class="btn btn-primary">Simpan data</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="btn btn-danger btn-sm text-white" href="{{url('transaksi/'.$data->id.'/delete')}}"><i class="ti ti-trash"></i></a>
                                                </div>
                                            </td>

                                        </tr>
                                        @php ++$i @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- @section('extra-scripts')
<script>
    function alertDelete(id) {
        console.log(id);
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data transaksi tidak akan bisa dipulihkan",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus'
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire(
                '',
                'Your file has been deleted.',
                'success'
                );
                window.location = "transaksi/"+id+"/delete";
            }
        })
    }
    const dateInput = document.getElementById("tanggalTransaksi");
    const dateInput2 = document.getElementById("tanggalTransaksi2");

    // Get today's date
    const today = new Date().toISOString().split("T")[0];

    // Set the min attribute of the date input to today's date
    dateInput.max = today;
    dateInput2.max = today;
</script>
@endsection --}}
