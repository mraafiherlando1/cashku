@extends('template.auth')

@section('title')
    CashKu Verify
@endsection
@section('content')
<div class="card mb-0">
              <div class="card-body">
                <a href="./index.html" class="text-nowrap logo-img text-center d-block py-3 w-100">
                  <img src="{{ asset('images/logos/logo.png') }}" width="180" alt="">
                </a>
                <p class="text-center">Jadi kaya bareng CashKu</p>
                <form method="POST" class="my-5" action="{{url('user/verify/process')}}">
                    @csrf
                    <p class="text-center">Masukkan kode verifikasi yang telah kami kirim ke email anda</p>
                  <div class="mb-3">
                    <input type="text" name="verification_code" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Kode Verifikasi">
                  </div>
                  <button type="submit" class="btn btn-primary w-100 py-8 fs-4 mb-4 rounded-2">Verifikasi</button>
                </form>
              </div>
            </div>
@endsection
