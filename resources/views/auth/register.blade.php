@extends('template.auth')

@section('title')
    CashKu Register
@endsection
@section('content')
    <div class="card mb-0">
        <div class="card-body">
            <a href="./index.html" class="text-nowrap logo-img text-center d-block py-3 w-100">
                <img src="{{ asset('images/logos/logo.png') }}
                                    " width="180"
                    alt="">
            </a>
            <p class="text-center">Jadi kaya bareng CashKu</p>
            <form method="POST" action="{{ url('register/process') }}">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputtext1" class="form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" id="exampleInputtext1"
                        aria-describedby="textHelp">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Email</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1"
                        aria-describedby="emailHelp">
                </div>
                <div class="mb-4">
                    <label for="exampleInputPassword1" class="form-label">Password</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                </div>
                <div class="mb-4">
                    <label for="exampleInputPassword1" class="form-label">Konfirmasi
                        Password</label>
                    <input type="password" name="password_confirmation" class="form-control" id="exampleInputPassword1">
                </div>
                <button type="submit" class="btn btn-primary w-100 py-8 fs-4 mb-4 rounded-2">Sign
                    Up</button>
                <div class="d-flex align-items-center justify-content-center">
                    <p class="fs-4 mb-0 fw-bold">Sudah memiliki akun?</p>
                    <a class="text-primary fw-bold ms-2" href="{{ url('login') }}">Sign
                        In</a>
                </div>
            </form>
        </div>
    </div>
@endsection
