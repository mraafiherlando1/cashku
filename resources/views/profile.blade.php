@extends('template.template')

@section('content')
@if (session('error'))
                <div class="response error">
                    <p class="text-center text-white my-auto">{{ session('error') }}</p>
                </div>
            @endif
            @if (session('success'))
                <div class="response success">
                    <p class="text-center text-white my-auto">{{ session('success') }}</p>
                </div>
            @endif
    <div class="row gutters-sm">
        <div class="d-flex gap-5">
            <div class="col-md-4 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <img src="{{$data->profile_picture == null ? 'https://bootdey.com/img/Content/avatar/avatar7.png' : 'storage/profile/'.$data->profile_picture}}" alt="Admin" class="rounded-circle"
                                width="150">
                            <div class="mt-3">
                                <h4>{{ $data->name }}</h4>
                                <p class="text-secondary mb-1">{{ $data->email }}</p>
                                <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#ubahFotoProfil">Ubah foto profil</button>
                                <div div class="modal fade" id="ubahFotoProfil" tabindex="-1" aria-labelledby="ubahFotoProfil"
                                    aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header d-flex flex-column">
                                                <div class="d-flex w-100 justify-content-between">
                                                    <h1 class="modal-title fs-5" id="exampleModalLabel">Ubah foto profil</h1>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                            </div>
                                            <div class="modal-body">
                                                <form id="formAccountSettings" method="POST"
                                                    action="{{ url('user/changepicture') }}" enctype="multipart/form-data">
                                                    <div class="row">
                                                        @csrf
                                                        <div class="mb-3" id="syaratBox">
                                                            <p class="form-label text-start">Foto</p>
                                                            <div class="d-flex" style="gap: 15px">
                                                                <input class="form-control mb-3" type="file" name="picture" id="syarat"
                                                                    placeholder="Syarat Permohonan" required />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                                    <button type="submit" class="btn btn-primary">Simpan foto</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Nama Lengkap</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                {{$data->name}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Email</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                {{$data->email}}
                            </div>
                        </div>
                        <hr>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex gap-4">
        <div class="col-lg-6">
            <div class="card">
                    <div class="card-body">
                        <h5 class="">Ubah Email</h5>
                        <p class="p-0 mb-4 fw-bold" style="color: gray">Anda harus melakukan verifikasi ulang jika mengubah email</p>
                        <form action="{{url('user/changeemail')}}" method="POST">
                            @csrf
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Email Lama</label>
                                <input type="email" class="form-control" name="old_email" value="{{$data->email}}" id="exampleInputEmail1" aria-describedby="emailHelp" disabled>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Email Baru</label>
                                <input type="email" class="form-control" name="new_email" id="exampleInputPassword1" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
            </div>
            
        </div>  
        <div class="col-lg-6">
            <div class="card">
                    <div class="card-body">
                        <h5 class="">Ubah Password</h5>
                        <p class="mb-4 text-light">p</p>
                        <form action="{{url('user/changepassword')}}" method="POST">
                            @csrf
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Password Lama</label>
                                <input type="password" name="old_password"class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Password Baru</label>
                                <input type="password" name="new_password"class="form-control" id="exampleInputPassword1" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
            </div>
            
        </div>  
    </div>
        
    </div>
@endsection
