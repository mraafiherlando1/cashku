@extends('template.template')

@section('content')
    <div class="row">
        @if (session('error'))
                <div class="response error">
                    <p class="text-center text-white my-auto">{{ session('error') }}</p>
                </div>
            @endif
            @if (session('success'))
                <div class="response success">
                    <p class="text-center text-white my-auto">{{ session('success') }}</p>
                </div>
            @endif
        <div class=" d-flex align-items-stretch">
            <div class="card w-100">
                <div class="card-body p-4">
                    <h5 class="card-title fw-semibold mb-4">Rincian Kategori</h5>
                    <div class="table-responsive">
                        @if(sizeof($kategori) == 0)
                        <p class="text-center">Tidak ada kategori yang terdaftar</p>
                        @else
                        <table class="table text-nowrap mb-0 align-middle">
                            <thead class="text-dark fs-4">
                                <tr>
                                    <th class="border-bottom-0">
                                        <h6 class="fw-semibold mb-0">No</h6>
                                    </th>
                                    <th class="border-bottom-0">
                                        <h6 class="fw-semibold mb-0">Nama Kategori</h6>
                                    </th>
                                    <th class="border-bottom-0">
                                        <h6 class="fw-semibold mb-0">Ikon</h6>
                                    </th>
                                    <th class="border-bottom-0">
                                        <h6 class="fw-semibold mb-0">Aksi</h6>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 1 @endphp
                                @foreach($kategori as $data)
                                <tr>
                                    <td class="border-bottom-0">
                                        <h6 class="fw-semibold mb-0">{{$i}}</h6>
                                    </td>
                                    <td class="border-bottom-0">
                                        <h6 class="fw-semibold mb-1">{{$data->nama_kategori}}</h6>
                                    </td>
                                    <td class="border-bottom-0">
                                        <img src="{{asset('storage/kategori/'.$data->icon)}}" style="width: 3em;" alt="ikon {{$data->nama_kategori}}">
                                    </td>
                                    <td class="border-bottom-0">
                                        <div class="d-flex align-items-center gap-2">
                                            <div class="dropdown">
                                                <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"
                                                    data-bs-target="#detail">
                                                    <i class="bx bx-dots-vertical-rounded"></i>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a class="dropdown-item" href="{{ url('kategori/' . $data->id) . '/delete' }}">Hapus kategori</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @php ++$i @endphp
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
                <div class="d-flex justify-content-center py-3">
                    <a class="btn btn-primary text-white" data-bs-toggle="modal" data-bs-target="#tambahFaqModal">Tambah data
                        baru</a>
                </div>
                <div class="modal fade" id="tambahFaqModal" tabindex="-1" aria-labelledby="tambahFaqModal" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="modal-title fs-5" id="exampleModalLabel">Tambah data kategori</h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form id="formLayananSettings" method="POST" action="{{ url('kategori/store') }}" enctype="multipart/form-data">
                                    <div class="row">
                                        @csrf
                                        <div class="mb-3">
                                            <label for="nama" class="form-label">Nama Kategori</label>
                                            <input class="form-control" type="text" name="nama_kategori" id="pertanyaan"
                                                placeholder="Nama Kategori" required/>
                                        </div>
                                        <div class="mb-3">
                                            <label for="deskripsi" class="form-label">Ikon</label>
                                            <input type="file" name="ikon" id="" class="form-control" required>
                                        </div>

                                    </div>
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                    <button type="submit" class="btn btn-primary">Simpan data</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
