@extends('template.template')

@section('content')
    <div class="row">
        @if (session('error'))
                <div class="response error">
                    <p class="text-center text-white my-auto">{{ session('error') }}</p>
                </div>
            @endif
            @if (session('success'))
                <div class="response success">
                    <p class="text-center text-white my-auto">{{ session('success') }}</p>
                </div>
            @endif
        <div class=" d-flex align-items-stretch">
            <div class="card w-100">
                <div class="card-body p-4">
                    <h5 class="card-title fw-semibold mb-4">Rincian Budgeting Bulanan</h5>

                    <p>Total Budget : Rp{{ number_format($total_budget, 2, ',', '.') }}</p>
                    <div class="table-responsive">
                        @if(sizeof($budgeting) == 0)
                        <p class="text-center">Tidak ada kategori yang terdaftar</p>
                        @else
                        <table class="table text-nowrap mb-0 align-middle">
                            <thead class="text-dark fs-4">
                                <tr>
                                    <th class="border-bottom-0">
                                        <h6 class="fw-semibold mb-0">No</h6>
                                    </th>
                                    <th class="border-bottom-0">
                                        <h6 class="fw-semibold mb-0">Nama Kategori</h6>
                                    </th>
                                    <th class="border-bottom-0">
                                        <h6 class="fw-semibold mb-0">Ikon</h6>
                                    </th>
                                    <th class="border-bottom-0">
                                        <h6 class="fw-semibold mb-0">Budget per Bulan</h6>
                                    </th>
                                    <th class="border-bottom-0">
                                        <h6 class="fw-semibold mb-0">Pengeluaran Bulan Ini</h6>
                                    </th>
                                    <th class="border-bottom-0">
                                        <h6 class="fw-semibold mb-0">Aksi</h6>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 1 @endphp
                                @foreach($budgeting as $data)
                                <tr>
                                    <td class="border-bottom-0">
                                        <h6 class="fw-semibold mb-0">{{$i}}</h6>
                                    </td>
                                    <td class="border-bottom-0">
                                        <h6 class="fw-semibold mb-1">{{$data->kategori->nama_kategori}}</h6>
                                    </td>
                                    <td class="border-bottom-0">
                                        <img src="{{asset('storage/kategori/'.$data->kategori->icon)}}" style="width: 3em;" alt="ikon {{$data->kategori->nama_kategori}}">
                                    </td>
                                    <td class="border-bottom-0">
                                        <h6 class="fw-semibold mb-1">Rp{{ number_format($data->jumlah_per_bulan, 2, ',', '.') }}</h6>
                                    </td>
                                    <td class="border-bottom-0">
                                        <h6 class="fw-semibold mb-1 {{$data->total_pengeluaran <= $data->jumlah_per_bulan ? 'text-success' : 'text-danger'}}">Rp{{ number_format($data->total_pengeluaran, 2, ',', '.') }}</h6>
                                    </td>
                                    <td class="border-bottom-0">
                                        <div class="d-flex">
                                            <a class="btn btn-primary btn-sm text-white" data-bs-toggle="modal" data-bs-target="#ubahBudget{{$i}}">Ubah budget</a>
                                        </div>
                                        <div class="modal fade" id="ubahBudget{{$i}}" tabindex="-1" aria-labelledby="ubahBudget{{$i}}" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h1 class="modal-title fs-5" id="exampleModalLabel">Ubah Budget {{$data->kategori->nama_kategori}}</h1>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form id="formLayananSettings" method="POST" action="{{ url('budgeting/'.$data->id.'/update') }}" enctype="multipart/form-data">
                                                            <div class="row">
                                                                @csrf
                                                                <div class="mb-3">
                                                                    <label for="nama" class="form-label">Jumlah Budget</label>
                                                                    <input class="form-control" type="number" name="jumlah_budget" id="pertanyaan"
                                                                        placeholder="Jumlah budget per bulan" value="{{$data->jumlah_per_bulan}}" required />
                                                                </div>
                                                            </div>
                                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                                            <button type="submit" class="btn btn-primary">Simpan data</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    
                                </tr>
                                @php ++$i @endphp
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
