@extends('template.template')

@section('content')
    <div class="row">
        <div class="col-lg-8 d-flex align-items-strech">
            <div class="card w-100">
                <div class="card-body">
                    <div class="d-sm-flex d-block align-items-center justify-content-between mb-9">
                        <div class="mb-3 mb-sm-0">
                            @php
                                $date = new DateTime(date('Y-m-d'));
                                setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
                                $formattedDate = strftime('%B %Y', $date->getTimestamp());
                            @endphp
                            <h5 class="card-title fw-semibold">Ringkasan Transaksi per Minggu - {{ $formattedDate }}</h5>
                        </div>
                    </div>
                    <canvas id="transactionsChart"></canvas>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="row">
                <div class="">
                    <!-- Monthly Earnings -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row alig n-items-start">
                                <div class="col-8">
                                    <h5 class="card-title mb-9 fw-semibold"> Pendapatan Bulan Ini </h5>
                                    <h4 class="fw-semibold mb-3">Rp{{ number_format($pendapatan, 2, ',', '.') }}</h4>
                                    <div class="d-flex align-items-center pb-1">
                                        <span
                                            class="me-2 rounded-circle bg-light-danger round-20 d-flex align-items-center justify-content-center">
                                            @if ($pendapatan >= $pendapatanBulanLalu)
                                                <i class="ti ti-arrow-up-right text-success"></i>
                                            @else
                                                <i class="ti ti-arrow-down-right text-danger"></i>
                                            @endif
                                        </span>
                                        @php
                                            $persentase = (($pendapatan - $pendapatanBulanLalu) / ($pendapatanBulanLalu == 0 ? $pendapatan : $pendapatanBulanLalu)) * 100;
                                        @endphp
                                        <p class="text-dark me-1 fs-3 mb-0">{{ $persentase }}%</p>
                                        <p class="fs-3 mb-0">dari bulan lalu</p>
                                        
                                    </div>
                                    <p> (Rp{{ number_format($pendapatanBulanLalu, 2, ',', '.') }})</p>
                                </div>
                                <div class="col-4">
                                    <div class="d-flex justify-content-end">
                                        <div
                                            class="text-white bg-secondary rounded-circle p-6 d-flex align-items-center justify-content-center">
                                            <i class="ti ti-currency-dollar fs-6"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class=" d-flex align-items-stretch">
            <div class="card w-100">
                <div class="card-body p-4">
                    <h5 class="card-title fw-semibold mb-4">Transaksi Terbaru</h5>
                    <div class="table-responsive">
                        <table class="table text-nowrap mb-0 align-middle">
                            <thead class="text-dark fs-4">
                                <tr>
                                    <th class="border-bottom-0">
                                        <h6 class="fw-semibold mb-0">No</h6>
                                    </th>
                                    <th class="border-bottom-0">
                                        <h6 class="fw-semibold mb-0">Kategori</h6>
                                    </th>
                                    <th class="border-bottom-0">
                                        <h6 class="fw-semibold mb-0">Tanggal Transaksi</h6>
                                    </th>
                                    <th class="border-bottom-0">
                                        <h6 class="fw-semibold mb-0">Jumlah</h6>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 1 @endphp
                                @foreach ($transaksi as $data)
                                    @php
                                        $date = new DateTime($data->tanggal_transaksi);
                                        setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
                                        $formattedDate = strftime('%e %B %Y', $date->getTimestamp());
                                    @endphp
                                    <tr>
                                        <td class="border-bottom-0">
                                            <h6 class="fw-semibold mb-0">{{ $i }}</h6>
                                        </td>
                                        <td class="border-bottom-0">
                                            <h6 class="fw-semibold mb-1">
                                                {{ $data->nama_kategori == null ? '-' : $data->nama_kategori }}</h6>
                                        </td>
                                        <td class="border-bottom-0">
                                            <p class="mb-0 fw-normal">{{ $formattedDate }}</p>
                                        </td>
                                        <td class="border-bottom-0">
                                            <h6
                                                class="fw-semibold mb-1 text-{{ $data->tipe == 'in' ? 'success' : 'danger' }}">
                                                +
                                                Rp{{ number_format($data->jumlah, 2, ',', '.') }}
                                            </h6>
                                        </td>
                                    </tr>
                                    @php if($i == 5) break; @endphp
                                    @php ++$i @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-scripts')
    <script>
        const dataIn = @json($dataMasuk);
        const dataOut = @json($dataKeluar);

        // Prepare the data for Chart.js
        const labels = Object.keys(dataIn);
        const valuesIn = Object.values(dataIn);
        const valuesOut = Object.values(dataOut);

        // Create the chart
        const ctx = document.getElementById('transactionsChart').getContext('2d');
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                        label: 'Transaksi Masuk',
                        data: valuesIn,
                        backgroundColor: 'rgba(75, 192, 192, 0.2)',
                        borderColor: 'rgba(75, 192, 192, 1)',
                        borderWidth: 1
                    },
                    {
                        label: 'Transaksi Keluar',
                        data: valuesOut,
                        backgroundColor: 'rgba(255, 99, 132, 0.2)',
                        borderColor: 'rgba(255, 99, 132, 1)',
                        borderWidth: 1
                    }
                ]
            },
            options: {
                responsive: true,
                scales: {
                    y: {
                        beginAtZero: true,
                        // Include a currency symbol and format the y-axis labels
                        ticks: {
                            callback: function(value, index, values) {
                                return 'Rp' + value.toLocaleString("id-ID");;
                            }
                        }
                    }
                }
            }
        });
    </script>
@endsection
